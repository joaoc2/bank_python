"""Desenvolva um programa que simule operações bancárias.
Permita que o usuário crie contas,
deposite dinheiro, faça saques e verifique o saldo.
Considere a inclusão de uma opção para transferência entre contas.
na lista: agencia fica na posicao 0, conta fica na posicao 1, senha fica na posicao 2 e saldo fica na posicao 3, usamos o conceito de list comprehension
"""

contas = [[],[],[],[]]
def criar(contas):
    contas[0].append(int(input("Agencia:")))
    contas[1].append(int(input("Conta:")))
    contas[2].append(int(input("Senha:")))
    contas[3].append(0.0)
    print(contas)


def deposito(contas):
    senha = int(input("Digite a senha da conta:"))
    if senha in contas[2]:
        for id in range(0,len(contas)):
            pos = contas[2].index(senha)


    valor = float(input("Valor a depositar?"))
    contas[-1][pos]+=valor
    print(contas)

def saldo(contas):
    senha = int(input("Digite a senha da conta:"))
    if senha in contas[2]:
        for id in range(0, len(contas)):
            pos = contas[2].index(senha)
    saldo = contas[-1][pos]
    print(f"Saldo:{saldo}")

def saque(contas):
    senha = int(input("Digite a senha da conta:"))
    if senha in contas[2]:
        for id in range(0, len(contas)):
            pos = contas[2].index(senha)
    valor = float(input("Valor do saque:"))
    contas[-1][pos]-=valor
    print(f"seu saldo atual é de {contas[-1][pos]}")

def deposito_contas(contas):
    senha = int(input("Digite a senha da conta:"))
    if senha in contas[2]:
        for id in range(0, len(contas)):
            pos = contas[2].index(senha)
    conta = int(input("Conta alvo:"))
    id_conta = contas[1].index(conta)

    valor = float(input("Valor da transferencia:"))
    contas[-1][pos]-=valor
    contas[-1][id_conta]+=valor
    print(contas)

while True:
    op = int(input("[1]Criar contas\n[2]Depositar\n[3]Saldo\n[4]Sacar\n[5]Deposito entre contas\nDigite a opcao:"))

    if op == 1:
        criar(contas)
    elif op == 2:
        deposito(contas)
    elif op == 3:
        saldo(contas)
    elif op == 4:
        saque(contas)
    elif op == 5:
        deposito_contas(contas)
    else:
        break